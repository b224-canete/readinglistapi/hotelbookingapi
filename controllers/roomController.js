const Room = require("../models/Room");

// Create new room
module.exports.addRoom = (reqBody) => {

    return Room.find({type: reqBody.type}).then(result => {
      
        if(result.length == 0) {
            let newRoom = new Room({

                type: reqBody.type,
                description: reqBody.description,
                sleeps: reqBody.sleeps,
                price: reqBody.price,
                units: reqBody.units,
                amenities: reqBody.amenities
            });

            return newRoom.save().then((room, error) => {
                if(error) {
                    return false;
                } else {
                    return room //"Room added in the system"
                }
            })
        } else {
            return "Room already exist. You can only update its details."
        }
    });
};


// Retrieve all rooms
module.exports.getAllRooms = () => {

    return Room.find({}).then(result => {

        if(result.length == 0) {
            return "No rooms added in the system";
        } else {
            return result;
        };
    });
};


// Retrieve all available roooms
module.exports.getAvailableRooms = () => {

    return Room.find({isAvailable: true}).then(result => {

        if(result.length == 0) {
            return "No available room";
        } else {
            return result;
        };
    });
};


// Retrieve specific room
module.exports.getRoom = (reqParams, data) => {

    return Room.findById(reqParams.roomId).then(result => {
        
        if(result == null) {
            return "Room does not exist";
        } else if(result.isAvailable == false && data == null) {
            return "Room is not available";
        } else if(result.isAvailable == false && data.isAdmin == false) {
            return "Room is not available";
        } else {
            return result;
        };
    });
};


// Update a room
module.exports.updateRoom = (reqParams, reqBody) => {

    let updatedRoom = {
        type: reqBody.type,
        description: reqBody.description,
        sleeps: reqBody.sleeps,
        price: reqBody.price,
        units: reqBody.units,
        amenities: reqBody.amenities 
    };

    Room.findByIdAndUpdate(reqParams.roomId, updatedRoom).then((updatedRoom, error) => {

        if(error) {
            return false;
        } else {
            return true;
        };
    });
    
    return Room.findById(reqParams.roomId).then(result => {
        
        if(result == null) {
            return {msg: "Room is non-existent"};
        } else {
            return result;
        };
    });
};


// Archive room
module.exports.archiveRoom = (reqParams) => {
    
    let archivedRoom = {
        isAvailable: false
    };

    return Room.findByIdAndUpdate(reqParams.roomId, archivedRoom).then((archivedRoom, error)=>{

        if(error){
            return false;
        } else {
            return {msg: "Success! Room has been archived."};
        };
    });
};


// Archive room
module.exports.activateRoom = (reqParams) => {
    
    let activatedRoom = {
        isAvailable: true
    };

    return Room.findByIdAndUpdate(reqParams.roomId, activatedRoom).then((activatedRoom, error)=>{

        if(error){
            return false;
        } else {
            return {msg: "Success! Room is activated."};
        };
    });
};