const Transaction = require("../models/Transaction");
const Room = require("../models/Room");


// Cancel Booking
module.exports.cancelBooking = (data, reqParams) => {
    
    return Transaction.findById(reqParams.transactionId).then( async transaction => {

        if(transaction == null || (transaction.status == "canceled")) {

            return {msg: "Invalid booking reference"};

        } else {

            if(data.isAdmin || (transaction.userId == data.id )) {

                transaction.status = "canceled";

                transaction.save().then((transaction, error) => {
                    if(error) {
                        return false;
                    } else {
                        return true;
                    };
                });

                let roomDetails = await Room.findById(transaction.roomId).then(result => {
                    if(result == null){
                        return false;
                    } else {
                        return result;
                    };
                });

                roomDetails.units += 1;

                roomDetails.save().then((room, error) => {
                    if(error){
                        return false;
                    } else {
                        return true;
                    };
                });

                return {msg: "Reservation is succesfully canceled."};

            } else {

                return {auth: "Invalid access. You are not allowed to cancel this booking."};
            };
        };
    });
};

// check-in guest booking
module.exports.checkIn = (reqBody) => {

    return Transaction.findById(reqBody.bookingId).then(result => {

        if(result == null) {

            return {msg: "Invalid booking reference."};
        } else {

            if(result.status != "reservation") {

                return {msg: "Invalid booking reference."};

            } else {

                return Transaction.findByIdAndUpdate(reqBody.bookingId, {status: "checked-in"}).then((status, error) => {

                    if(error) {
                        return {msg: "Check-In failed. Please try again."};
                    } else {
                        return {msg: "Succesfully checked-in guest."};
                    };
                });
            };
        };
    });
};


// check-out guest booking
module.exports.checkOut = (reqBody) => {

    return Transaction.findById(reqBody.bookingId).then(booking => {
       
        if(booking == null) {

            return {msg: "Invalid booking reference."};
        } else {
            
            if(booking.status != "checked-in") {

                return {msg: "Invalid booking reference."};

            } else {

                return Transaction.findByIdAndUpdate(reqBody.bookingId, {status: "checked-out"}).then(async (status, error) => {

                    if(error) {
                        return {msg: "Check-out failed. Please try again."};
                    } else {

                        let roomDetails = await Room.findById(booking.roomId).then(result => {
                            if(result == null) {
                                return false;
                            } else {
                                return result;
                            };
                        });

                        roomDetails.units += 1;
                        
                        roomDetails.save().then((room, error) => {
                            
                            if(error) {
                                return false;
                            } else {
                                return true;
                            };
                        });

                        return {msg: "Succesfully checked-out guest."};
                    };
                });
            };
        };
    });
};


// Retrieve Transactions
module.exports.getTransactions = (data, reqBody) => {

    if(data.isAdmin && reqBody.status == "all") {

        return Transaction.find({}).then(result => {

            if(result.length == 0) {

                return {msg: "No transaction yet."};
            } else {
                return result;
            };
        });
    } else if(data.isAdmin) {

        return Transaction.find({status: reqBody.status}).then(result => {

            if(result.length == 0) {

                return {msg: "No transaction currently in the database."};
            } else {
                return result;
            };
        });
    } else if(!data.isAdmin && reqBody.status == "all") {

        return Transaction.find({userId: data.id}).then(result => {

            if(result.length == 0) {

                return {msg: "No transaction currently in the database."};
            } else {
                return result;
            };
        });
    } else {
        
        return Transaction.find({userId: data.id , status: reqBody.status}).then(result => {

            if(result.length == 0) {

                return {msg: "No transaction currently in the database."};
            } else {
                return result;
            };
        });
    };
};









