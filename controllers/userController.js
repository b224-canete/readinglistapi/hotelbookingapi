const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Room = require("../models/Room");
const Transaction = require("../models/Transaction");



// User registration
module.exports.registerUser = (reqBody) => {

    return User.find({email: reqBody.email}).then(async result => {
        
        if(result.length > 0) {

            return {msg: "Sorry, email already exist."};
            
        } else {

            let newUser = new User({
                fullName: reqBody.fullName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                password: bcrypt.hashSync(reqBody.password, 10)
            });

            let userCount = await User.find({}).then(result => {
                
                if(result == 0) {
                    return result;
                } else {
                    return result;
                };
            });
            
            if(userCount.length == 0) {
                newUser.isAdmin = true,
                newUser.isMainAdmin =true
            };
              
            return newUser.save().then((user, error) => {

                if(error) {
                    return {msg: "Registration failed. Please try again"};
                } else {
                    return {msg: `Hi ${reqBody.fullName}. You have successfully registered.`};
                };
            });
        };
    });
};


// User login
module.exports.login = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {

            return {msg: "Email does not exists"};

        } else {
            
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){

                return {access: auth.createAccessToken(result)};
                
            } else {

                return {msg: "Incorrect password. Please try again"};
            };
        };
    });
};


// Update user details
module.exports.updateUserDetails = (data, reqBody) => {

    console.log(data);

    let updatedUser = {
        fullName: reqBody.fullName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo
    };

    return User.find({email: reqBody.email}).then( result => {
        
        if(result.length > 0) {

            return {msg: "Sorry, email already exist."};

        } else {

            return User.findByIdAndUpdate(data.id, updatedUser).then((updatedUser, error) => {

                if(error) {
                    return {msg: "Update failed. Please try again."};
                } else {
                    return { msg: "Update successful!"};
                };
            });
        };
    });    
};


// Retrieve user Details
module.exports.getUser = (data) => {

    return User.findById(data.id, {password: 0}).then(result => {

        if(result == null) {
            return false;
        } else {
            return result;
        };
    });
};


// Set as Admin
module.exports.addAdmin = (reqBody) => {

    let userAdmin = {
        isAdmin: true
    };

    return User.findByIdAndUpdate(reqBody.userId, userAdmin).then((userAdmin, error) => {

        if(error){
            return {msg: "Update failed"};
        } else {
            
            if(userAdmin.isAdmin) {
                return {msg: "User is already an Admin."};
            } else {
                return {msg: "User is succesfully added as an admin."};
            };
        };
    });
};


// Create a booking transaction
module.exports.addBooking = async (data, reqBody) => {

    let roomDetails = await Room.findById(reqBody.roomId).then(result => {

        if(result == null) {
            return false;
        } else {
            return result;
        };
    });

    if(roomDetails.isAvailable) {

            let newBooking = new Transaction({
                userId: data.id,
                fullName: data.fullName,
                roomId: reqBody.roomId,
                type: roomDetails.type,
                modeOfPayment: reqBody.modeOfPayment
            });

            return newBooking.save().then((booking, error) => {
                
                if(error) {
                    return {msg: "Booking failed. Please try again."};
                } else {

                    roomDetails.units -= 1;

                    if(roomDetails.units == 0) {
                        roomDetails.isAvailable = false
                    }

                    roomDetails.save().then((room, error) => {

                        if(error){
                            return false;
                        } else {
                            return true;
                        };
                    });

                    return {msg: "Booking success! Room is reserved."}
                };
            });

    } else {
        return {msg: "Sorry, room is not available for reservation"};
    };
};