const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const roomRoutes = require("./routes/roomRoutes");
const transactionRoutes = require("./routes/transactionRoutes");


const app = express();
const port = process.env.PORT || 4000;
mongoose.set('strictQuery', false);

mongoose.connect(`mongodb+srv://lhemarcanete:admin123@224canete.vd2heps.mongodb.net/Hotel-Booking-API?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on(`error`, () => console.error(`Connection error.`));
db.once(`open`, () => console.log(`Connected to MongoDB`));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use("/users", userRoutes);
app.use("/rooms", roomRoutes);
app.use("/transactions", transactionRoutes);

app.listen(port, () => {console.log(`API is now running at port: ${port}`)});