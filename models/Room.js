const mongoose = require("mongoose");

const roomSchema = new mongoose.Schema({
    type: {
        type: String,
        required: [true, "Room type is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    isAvailable: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    sleeps: {
        type: String,
        required: [true, "Head count fits in the room is required"]
    },
    price: {
        type: Number,
        required: [true, "Price per night is required"]
    },
    units: {
        type: Number,
        required: [true, "Available units is required"]
    },
    amenities: {
        type: String,
        required: [true, "Room amenities is required"]
    }
});

module.exports = mongoose.model("Room", roomSchema)

