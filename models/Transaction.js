const mongoose = require("mongoose");

const transactionSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "UserId is required"]
    },
    fullName: {
        type: String,
        required: [true, "User name is required"]
    },
    roomId: {
        type: String,
        required: [true, "RoomId is required"]
    },
    type: {
        type: String,
        required: [true, "Room type is required"]
    },
    status: {
        type: String,
        default: 'reservation' //reservation, checked-in, checked-out
    },
    reservationDate: {
        type: Date,
        default: new Date()
    },
    checkedInDate: {
        type: Date
    },
    checkedOutDate: {
        type: Date
    },
    modeOfPayment: {
        type: String,
        required: [true, "Mode of payment is required"]
    }
});

module.exports = mongoose.model("Transaction", transactionSchema);