const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    fullName: {
        type : String,
        requires: [true, "Your name is required"]
    },
    email: {
        type: String,
        required: [true, "Your email is required"]
    },
    password: {
        type: String,
        required: [true, "Input a password is a must"]
    },    
    isAdmin: {
        type: Boolean,
        default: false
    },
    isMainAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "You are required to give your contact#"]
    },
    registeredOn: {
        type: Date,
        default: new Date()
    }
})


module.exports = mongoose.model("User", userSchema);