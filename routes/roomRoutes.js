const express = require("express");
const router = express.Router();
const roomController = require("../controllers/roomController");
const auth = require("../auth");


// Route for creating a room 
router.post("/create", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        roomController.addRoom(req.body).then  (resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});


// Route for retrieving all rooms
router.get("/allRooms", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        roomController.getAllRooms().then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});


// Route for getting all available rooms
router.get("/rooms", auth.verify, (req, res) => {

    roomController.getAvailableRooms().then(resultFromController => res.send(resultFromController));
});





// WITH PARAMS

// Route for retrieving a single room
router.get("/:roomId", (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    roomController.getRoom(req.params, userData).then(resultFromController => res.send(resultFromController));
});


// Route for updating room information
router.put("/:roomId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

        roomController.updateRoom(req.params, req.body).then(resultFromController => res.send(resultFromController));
    
    } else {

        res.send({auth: "failed"});
    };
});


// Route for archiving room
router.put("/:roomId/archive", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

    roomController.archiveRoom(req.params).then(resultFromController => res.send(resultFromController));

    } else {
        
        res.send({auth: "Unauthorized to access this route"});
    };
});

//Route for activate room
router.put("/:roomId/activate", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

    roomController.activateRoom(req.params).then(resultFromController => res.send(resultFromController));

    } else {

        res.send({auth: "Unauthorized to access this route"});
    };
});







module.exports = router;
