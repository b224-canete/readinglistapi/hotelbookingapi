const express = require("express");
const router = express.Router();
const auth = require("../auth");
const transactionController = require("../controllers/transactionController");


// Route to cancel a booking
router.patch("/:transactionId/cancel", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    transactionController.cancelBooking(userData, req.params).then(resultFromController => res.send(resultFromController));
});


// Route for check-in
router.patch("/checkIn", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {

        transactionController.checkIn(req.body).then(resultFromController => res.send(resultFromController));

    } else {

        res.send({auth: "Unauthorized access."});
    };
});


// Route for check-out
router.patch("/checkOut", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin) {

        transactionController.checkOut(req.body).then(resultFromController => res.send(resultFromController));

    } else {

        res.send({auth: "Unauthorized access."});
    };
});


// Retrieve Transactions
router.get("/list", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    transactionController.getTransactions(userData, req.body).then(resultFromController => res.send(resultFromController));
});







module.exports = router;