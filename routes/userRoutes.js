const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");



// Route for User Registration
router.post("/register", (req, res) => {
 
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for Login
router.post("/login", (req, res) => {

    userController.login(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for updating user information
router.put("/update", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    userController.updateUserDetails(userData, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    userController.getUser(userData).then(resultFromController => res.send(resultFromController));
});


// Route for setting user as admin
router.put("/addAdmin", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {

        userController.addAdmin(req.body).then(resultFromController => res.send(resultFromController));

    } else {

        res.send({auth: "You're unauthorized to access this route"});
    };
});


// Route for user booking
router.post("/booking", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        
        res.send({auth: "Admin can't book a reservation"});

    } else {
       
        userController.addBooking(userData, req.body).then(resultFromController => res.send(resultFromController));
    };
});










module.exports = router;